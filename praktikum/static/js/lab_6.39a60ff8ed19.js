$(document).ready(function() {
	themes = [{"id": 0, "text": "Red", "bcgColor": "#F44336", "fontColor": "#FAFAFA"},
    {"id": 1, "text": "Pink", "bcgColor": "#E91E63", "fontColor": "#FAFAFA"},
    {"id": 2, "text": "Purple", "bcgColor": "#9C27B0", "fontColor": "#FAFAFA"},
    {"id": 3, "text": "Indigo", "bcgColor": "#3F51B5", "fontColor": "#FAFAFA"},
    {"id": 4, "text": "Blue", "bcgColor": "#2196F3", "fontColor": "#212121"},
    {"id": 5, "text": "Teal", "bcgColor": "#009688", "fontColor": "#212121"},
    {"id": 6, "text": "Lime", "bcgColor": "#CDDC39", "fontColor": "#212121"},
    {"id": 7, "text": "Yellow", "bcgColor": "#FFEB3B", "fontColor": "#212121"},
    {"id": 8, "text": "Amber", "bcgColor": "#FFC107", "fontColor": "#212121"},
    {"id": 9, "text": "Orange", "bcgColor": "#FF5722", "fontColor": "#212121"},
    {"id": 10, "text": "Brown", "bcgColor": "#795548", "fontColor": "#FAFAFA"}];
	
	localStorage.setItem("themes", JSON.stringify(themes));
	
	// My select initiate
	 mySelect = $('.my-select').select2();
	
	$('.my-select').select2({
		'data': JSON.parse(localStorage.getItem("themes"))
	});
	// Save before reload theme
	var kumpulanTheme = JSON.parse(localStorage.getItem("themes"));
	var indigoChoose = kumpulanTheme[3];
	var defaultTema = indigoChoose;
	var selectTema = indigoChoose;
	var chacedTheme = indigoChoose;
    selectTema = chacedTheme;
	$('body').css(
        {
            "background-color": selectTema.bcgColor,
            "font-color": selectTema.fontColor
        }
    );
	
	// Apply Button
	$('.apply-button').on('click', function(){  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select
	var selectTheme = mySelect.val();
	console.log(selectTheme);
    // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada dan ambil object theme yang dipilih
	if (selectTheme < kumpulanTheme.length) {
		selectTema = kumpulanTheme[selectTheme];
	}

    // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
	$('body').css(
            {
                "background-color": selectTema.bcgColor,
                "font-color": selectTema.fontColor
            }
        );
    // [TODO] simpan object theme tadi ke local storage selectedTheme
	localStorage.setItem("selectTema", JSON.stringify(selectTema));
    })
	
	
	// Chat Box Sederhana
	var count = 0;
    //nambahin chat text setelah di tekan 'enter'
    $('#chat-text').keypress(function (tekan) {
        if (tekan.keyCode == 13 || tekan.which == 13) {
            var pesan = $('#chat-text').val();
			$("#chat-text").val('');
            if (pesan.length === 0) {
                alert("Message kosong");
            }
            else {
                // insert new chat to html
				var chatClass = (count == 0 ? 'msg-send' : 'msg-receive');
				count = (count ? 0 : 1);
				var newChat = $('<div class="' + chatClass + '">' + pesan + '</div>')
				$(".msg-insert").append(newChat);
            }
        }
    });
    //END
});

// Calculator
var print = document.getElementById('print');
var erase = false;
var go = function(x) {
  if (x === 'ac') {
  	print.value = "";
    /* implemetnasi clear all */
  } else if (x === 'eval') {
	if (print.value.includes('sin') || print.value.includes('tan') || print.value.includes('log')) {
		var penandaAwal = print.value.indexOf('(');
		var penandaAkhir = print.value.indexOf(')');
		if (print.value.includes('sin')) {
			print.value = Math.round(Math.sin(print.value.substring(penandaAwal+1, penandaAkhir)*Math.PI/180)*10000)/10000;
		} else if (print.value.includes('tan')) {
			print.value = Math.round(Math.tan(print.value.substring(penandaAwal+1, penandaAkhir)*Math.PI/180)*10000)/10000;
		} else if (print.value.includes('log')) {
			print.value = Math.log10(print.value.substring(penandaAwal+1, penandaAkhir));
		}
		erase = true;
	}
	else{
		print.value = Math.round(evil(print.value) * 10000) / 10000;
		erase = true;
	 }
  } 
  else {
	if (erase) {
		print.value = "";
		erase = false;
	}
    print.value += x;
  }
};
function evil(fn) {
  return new Function('return ' + fn)();
}
// END


