// JavaScript
// Untuk menjalankan secara asincronous
 window.fbAsyncInit = function() {
    FB.init({
      appId      : '348028152325974',
      cookie     : true,
      xfbml      : true,
      version    : 'v2.11'
    });
  };

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "https://connect.facebook.net/id_ID/sdk.js#xfbml=1&version=v2.11&appId=348028152325974";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
   
// Memanggil fungsi login dari API milik facebook dan bisa membuat post ke timeline
   function facebookLogin(){
     FB.login(function(response){
       console.log(response);
     }, {scope:'public_profile,user_posts,publish_actions'})
   }
// Menampilkan informasi user
	function getUserData(){
	FB.getLoginStatus(function(response) {
        if (response.status === 'connected') {
          FB.api('/me?fields=id,name', 'GET', function(response){
            console.log(response);
			});
		   }
	   });
	}
